package app26;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;

public class HttpClientDemo1 {

    public static void main(String[] args) {
        HttpClient httpClient = HttpClient.newHttpClient();
        String uri = "https://www.google.com";
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder().uri(
                    new URI(uri)).GET().build();
            HttpResponse<String> httpResponse = httpClient.send(
                    httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(httpResponse.statusCode());
            System.out.println(httpResponse.body());
            HttpHeaders headers = httpResponse.headers();
            Map<String, List<String>> map = headers.map();
            for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                System.out.println(entry.getKey() + ":" 
                        + entry.getValue());
            }
        } catch (IOException | URISyntaxException 
                | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
