package app26;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpClientRedirectDemo {

    public static void main(String[] args) {
        HttpClient.Builder httpClientBuilder = HttpClient.newBuilder()
                .followRedirects(HttpClient.Redirect.ALWAYS);
        HttpClient httpClient = httpClientBuilder.build();
        String uri = "http://google.com";
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder().uri(
                    new URI(uri)).GET().build();
            System.out.println("request uri:" + httpRequest.uri());
            HttpResponse<String> httpResponse = httpClient
                .send(
                    httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println("response uri:" + httpResponse.uri());
            System.out.println(httpResponse.statusCode());
        } catch (IOException | URISyntaxException 
                | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
