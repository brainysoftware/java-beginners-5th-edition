package app26;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpClientDemo2 {
   public static void main(String[] args) {
       HttpClient httpClient = HttpClient.newHttpClient();
       String uri = "http://localhost:8080/test/test2.jsp";
       try {
           HttpRequest httpRequest = HttpRequest.newBuilder()
                   .uri(new URI(uri))
                   .header("User-Agent", "Java HttpClient API")
                   .header("Content-Type", 
                           "application/x-www-form-urlencoded")
                   .POST(HttpRequest.BodyPublishers.ofString(
                           "firstName=Charles&lastName=Darwin"))
                   .build();
           HttpResponse<String> httpResponse = httpClient.send(
                   httpRequest, HttpResponse.BodyHandlers.ofString());
           System.out.println(httpResponse.body());
       } catch (IOException | URISyntaxException 
               | InterruptedException e) {
           e.printStackTrace();
       }
    }
}