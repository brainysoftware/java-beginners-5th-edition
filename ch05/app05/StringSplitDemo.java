package app05;

import java.util.StringTokenizer;

public class StringSplitDemo {
    public static void main(String[] args) {
        String text = "Energy   efficient car";
        String[] tokens = text.split(" ");
        System.out.format("Number of tokens (String.split()): %d\n", 
                tokens.length);
        System.out.println("Tokens (String.split()):");
        for (String token : tokens) {
            System.out.println("- " + token);
        }

        System.out.println("=====================================");
        tokens = text.split("\\s+");
        System.out.format("Number of tokens (String.split()): %d\n", 
                tokens.length);
        System.out.println("Tokens (String.split()):");
        for (String token : tokens) {
            System.out.println("- " + token);
        }
        
        System.out.println("=====================================");
        StringTokenizer stringTokenizer = new StringTokenizer(text, " ");
        System.out.format("Number of tokens (StringTokenizer): %d\n", 
                stringTokenizer.countTokens());
        System.out.println("Tokens (StringTokenizer):");
        while (stringTokenizer.hasMoreTokens()) {
        	System.out.println("- " + stringTokenizer.nextToken());
        }
    }
}
