package app24;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ThreadPriorityDemo extends Application {
	
	List<Label> labels = new ArrayList<>();
	int numThreads = 10;

    class CounterThread extends Thread {
        Label counterLabel;
        public CounterThread(Label counterLabel) {
            this.counterLabel = counterLabel;
        }

        public void run() {
            for (int i = 0; i < 50_000; i++) {
                final int count = i;
                Platform.runLater(() -> counterLabel.setText(Integer.toString(count)));
            	try {
            		Thread.sleep(1);
            	} catch (InterruptedException e) {
            	}
            }
        }
    }

    @Override
    public void start(Stage stage) {
        HBox root = new HBox(40); 
        ObservableList<Node> children = root.getChildren();
        Scene scene = new Scene(root, 600, 25, Color.WHITESMOKE);
        for (int i = 0; i < numThreads; i++) {
        	labels.add(new Label("     "));
        }
        children.addAll(labels);
        stage.setTitle("Thread Priority Demo");
        stage.setScene(scene);
        stage.show();
        
        CounterThread[] threads = new CounterThread[numThreads];
        for (int i = 0; i < numThreads; i++) {
            threads[i] = new CounterThread(labels.get(i));
            threads[i].setPriority(i >= numThreads / 2 ? 10 : 1);
        }
        for (int i = 0; i < numThreads; i++) {
        	threads[i].start();
        }
    }

    public static void main(String[] args) {
    	launch(args);
    }
}