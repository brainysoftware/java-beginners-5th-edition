package app24;
import java.awt.Toolkit;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TimerDemo extends Application {
    String[] questions = { "What is the largest mammal?",
            "Who is the current prime minister of Japan?",
            "Who invented the Internet?",
            "What is the smallest country in the world?",
            "What is the biggest city in America?",
            "Finished. Please remain seated" };

    Label questionLabel = new Label();
    TextField answer = new TextField("");
    Button startButton = new Button("Start");
    int counter = 0;
    Timer timer = new Timer();
    ListView<String> answerListView = new ListView<String>(); 

    @Override
    public void start(Stage stage) {
    	VBox root = new VBox(4);
    	root.setPadding(new Insets(2, 2, 2, 2));
        ObservableList<Node> children = root.getChildren();
        HBox hBox = new HBox(2);
        hBox.getChildren().addAll(new Label("Click Start"), startButton);
        children.addAll(hBox, questionLabel, answer, answerListView);
        startButton.setOnAction(new EventHandler<ActionEvent>() { 
            @Override 
            public void handle(ActionEvent event) {
            	Platform.runLater(() -> startButton.setDisable(true));
            	timer.schedule(new DisplayQuestionTask(), 0, 5 * 1000);
            } 
        }); 
        Scene scene = new Scene(root, 600, 200);
        stage.setTitle("Timer Demo");
        stage.setScene(scene); 
        stage.show(); 
    }
    
    @Override
    public void stop() {
    	timer.cancel();
    }

    public static void main(String[] args) {
    	launch(args);
    }

    private String getNextQuestion() {
        return questions[counter++];
    }

    private class DisplayQuestionTask extends TimerTask {
        @Override
        public void run() {
            Toolkit.getDefaultToolkit().beep();
            String nextQuestion = getNextQuestion();
        	Platform.runLater(() -> {
                if (answer.getText().trim().length() > 0) {
                    answerListView.getItems().add(answer.getText()); 
                    answer.setText("");
                }
                questionLabel.setText(nextQuestion);
        	});
            if (counter == questions.length) {
                timer.cancel();
            }    
        }
    }
}