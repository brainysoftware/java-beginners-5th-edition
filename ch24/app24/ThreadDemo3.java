package app24;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ThreadDemo3 extends Application {
    Label countUpLabel = new Label("    ");
    Label countDownLabel = new Label("    ");

    class CountUpThread extends Thread {
    	@Override
        public void run() {
			for (int i = 0; i < 100; i++) {
				final int count = i;
                Platform.runLater(() -> countUpLabel.setText(Integer.toString(count)));
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
				}
			}
        }
    }

    class CountDownThread extends Thread {
    	@Override
        public void run() {
			for (int i = 100; i >= 0; i--) {
				final int count = i;
                Platform.runLater(() -> countDownLabel.setText(Integer.toString(count)));
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
			}
        }
    }
    
    @Override
    public void start(Stage stage) {
        HBox root = new HBox(40); 
        ObservableList<Node> children = root.getChildren();
        Scene scene = new Scene(root, 250, 25, Color.WHITESMOKE);
        children.addAll(countUpLabel, countDownLabel);
        stage.setTitle("Counters");
        stage.setScene(scene);
        stage.show();
    	new CountUpThread().start();
    	new CountDownThread().start();
    }

    public static void main(String[] args) {
    	launch(args);
    }
}