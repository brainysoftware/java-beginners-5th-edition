package app24;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class StopThreadDemo extends Application {
    private Label counterLabel = new Label("Counter");
    private Button startButton = new Button("Start");
    private Button stopButton = new Button("Stop");
    private CounterThread thread;
    private boolean stopped = false;
    private int count = 0;

    class CounterThread extends Thread {
        public void run() {
            while (!stopped) {
                try {
                    sleep(10);
                } catch (InterruptedException e) {
                }
                int count2 = count++;
            	Platform.runLater(() -> counterLabel.setText(Integer.toString(count2)));
            }
        }
    }

    public void start(Stage stage) {
        HBox root = new HBox(40); 
        ObservableList<Node> children = root.getChildren();
        Scene scene = new Scene(root, 250, 25, Color.WHITESMOKE);
        children.addAll(counterLabel, startButton, stopButton);
        stage.setTitle("Stop Thread Demo");
        stage.setScene(scene);
        startButton.setOnAction(new EventHandler<ActionEvent>() { 
            @Override 
            public void handle(ActionEvent event) { 
            	Platform.runLater(()-> {
            		startButton.setDisable(true);
            		stopButton.setDisable(false);
            	});
                startThread();
            } 
        }); 
        stopButton.setOnAction(new EventHandler<ActionEvent>() { 
            @Override 
            public void handle(ActionEvent event) {
            	Platform.runLater(()-> {
            		startButton.setDisable(false);
            		stopButton.setDisable(true);
            	});
                stopThread();
            } 
        }); 
        stage.show();
    }

    public synchronized void startThread() {
        stopped = false;
        thread = new CounterThread();
        thread.start();
    }
    
    public synchronized void stopThread() {
        stopped = true;
    }

    public static void main(String[] args) {
    	launch(args);
    }
}