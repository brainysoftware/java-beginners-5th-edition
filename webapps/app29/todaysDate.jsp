<%@page import="java.time.LocalDate"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.format.FormatStyle"%>
<!DOCTYPE html>
<html>
<head><title>Today's date</title></head>
<body>
<%
  LocalDate today = LocalDate.now();
  String s = today.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG));
  out.println("Today is " + s);
%>
</body>
</html>