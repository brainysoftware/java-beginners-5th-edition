package app19;

public class CalculatorTest {
	public static void main(String[] args) {
		Calculator addition = (int a, int b) -> (a + b);
		System.out.println(addition.calculate(5, 20)); // prints 25.0

		addition = (a, b) -> (a + b);
		System.out.println(addition.calculate(5, 20)); // prints 25.0

	}
}
