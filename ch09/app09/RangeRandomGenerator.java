package app09;
import java.util.concurrent.ThreadLocalRandom;

public class RangeRandomGenerator {

    private int from;
    private int to;
    
    /*
     * Returns a random number between 'from' and 'to' (inclusive)
     */
    public int generate() {
    	return ThreadLocalRandom.current().nextInt(from, to + 1);
    }

    public RangeRandomGenerator(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public static void main(String[] args) {
        RangeRandomGenerator generator = 
                new RangeRandomGenerator(5, 20);
        for (int i = 0; i < 20; i++) {
            System.out.print(generator.generate() + " ");
        }
    }
}