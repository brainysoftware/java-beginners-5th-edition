package app09;

import java.util.concurrent.ThreadLocalRandom;

public class RandomNumberGenerator {
    public static void main(String[] args) {
    	ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = 0; i < 30; i++) {
            // Print a random number between 50 and 59 (inclusive)
            System.out.print(random.nextInt(50, 60) + " ");
        }
    }
}