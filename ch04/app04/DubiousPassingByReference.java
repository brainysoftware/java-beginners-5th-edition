package app04;

public class DubiousPassingByReference {
	
    public static void swap(Integer a, Integer b) {
	    Integer temp = a;
	    a = b;
	    b = temp;
    }
    public static void main(String[] args) {
        Integer x = 1;
        Integer y = -1;
        swap(x, y);
        System.out.println(x); // prints 1
    }
}