package app21.dao;

import java.util.List;
import app21.model.Product;

public interface ProductDAO extends DAO {
    List<Product> getProducts() throws DAOException;
    void insert(Product product) throws DAOException;
}