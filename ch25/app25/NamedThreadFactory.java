package app25;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class NamedThreadFactory implements ThreadFactory {
    private final ThreadGroup group;
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    private final String name;

    public NamedThreadFactory(String name) {
        SecurityManager s = System.getSecurityManager();
        group = s != null ? s.getThreadGroup() :
                Thread.currentThread().getThreadGroup();
        this.name = name;
    }

    public Thread newThread(Runnable runnable) {
    	return new Thread(group, runnable, 
                name + threadNumber.getAndIncrement(), 0);
    }
}
