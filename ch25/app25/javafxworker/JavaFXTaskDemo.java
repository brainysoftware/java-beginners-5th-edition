package app25.javafxworker;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class JavaFXTaskDemo extends Application {
    private CounterTask task;
    private ExecutorService executorService = Executors.newFixedThreadPool(2);
    
    public void start(Stage stage) {
        
        VBox root = new VBox(4); 
        ObservableList<Node> children = root.getChildren();
        HBox hbox = new HBox(4);
        hbox.setStyle("-fx-background-color: #DEDEDE;-fx-margin:5");
        hbox.setPadding(new Insets(8, 12, 8, 12));
        hbox.setSpacing(10);
        
        ProgressBar progressBar = new ProgressBar();
        //stretch the progressBar's width to match parent's width
        progressBar.prefWidthProperty().bind(root.widthProperty());
        TextField textField = new TextField();
        textField.setFont(Font.font("Verdana", FontWeight.BOLD, 15));
        
        Button startButton = new Button("Start");
        startButton.setOnAction(new EventHandler<ActionEvent>() { 
            @Override 
            public void handle(ActionEvent event) {
                task = new CounterTask();
                progressBar.progressProperty().bind(task.progressProperty());
                textField.textProperty().bind(task.messageProperty());
                executorService.submit(task);
            } 
        }); 

        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(new EventHandler<ActionEvent>() { 
            @Override 
            public void handle(ActionEvent event) {
                task.cancel();
            } 
        }); 

        hbox.getChildren().addAll(startButton, cancelButton);
        children.addAll(hbox, textField, progressBar); 

        Scene scene = new Scene(root, 280, 100);
        stage.setTitle("JavaFX Task Demo"); 
        stage.setScene(scene); 
        stage.show(); 
    }
    
    @Override
    public void stop() {
        executorService.shutdownNow();
    }

    public static void main(String[] args) {
        launch();
    }
}