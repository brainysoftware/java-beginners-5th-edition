package app25.javafxworker;
import javafx.concurrent.Task;

public class CounterTask extends Task<Void> {
    private static final int DELAY = 1000;
    @Override
    protected Void call() throws Exception {
        int count = 10;
        for (int i = 1; i <= count; i++) {
            if (isCancelled()) {
                updateMessage("Cancelled!");
                break;
            }
            updateMessage(Integer.toString(i));
            updateProgress(i, count);
            try {
                Thread.sleep(DELAY);
            } catch (InterruptedException e) {
                updateMessage("Cancelled while sleeping");
                return null;
            }
        }
        updateMessage("Done!");
        return null;
    }
 }