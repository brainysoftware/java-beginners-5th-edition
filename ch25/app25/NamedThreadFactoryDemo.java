package app25;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NamedThreadFactoryDemo {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(10,
                new NamedThreadFactory("file-finder-thread"));
        Runnable runnable = () -> {
            for (int i = 0; i < 100; i++) {
                if (Thread.interrupted()) {
                    break;
                }
                System.out.println(i);
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                }
            }
        };
        executorService.submit(runnable);
        executorService.shutdown();
    }
}
