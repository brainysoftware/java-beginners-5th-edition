package app25.imagesearch;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Executor;

public class ImageSearchTask implements Runnable {
    private Path searchDir;
    private Executor executor;
    private FileFoundListener fileFoundListener = null;

    public ImageSearchTask(Path searchDir, Executor executor) {
        this.searchDir = searchDir;
        this.executor = executor;
    }

    @Override
    public void run() {
        try (DirectoryStream<Path> children = 
                Files.newDirectoryStream(searchDir)) {
            for (final Path child : children) {
                if (Files.isDirectory(child)) {
                	ImageSearchTask task = new ImageSearchTask(
                            child, executor);
                	task.setFileFoundListener(fileFoundListener);
                    executor.execute(task);
                } else if (Files.isRegularFile(child)) {
                    String name = child.getFileName()
                            .toString().toLowerCase();
                    if (name.endsWith(".jpg") || name.endsWith(".png") 
                            || name.endsWith(".bmp")) {
                    	triggerFileFoundEvent(child);
                    }
                }
            }            
        } catch (AccessDeniedException e) {
            System.out.println("AccessDeniedException : " 
                    + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException : " + e.getMessage());
        }
    }
    
    public void setFileFoundListener(FileFoundListener fileFoundListener) {
        this.fileFoundListener = fileFoundListener;
    }
    
    private void triggerFileFoundEvent(Path path) {
    	if (fileFoundListener != null) {
    		fileFoundListener.fileFound(new FileFoundEvent(this, path));
    	}
    }
}