package app25.imagesearch;

import java.nio.file.Path;
import java.util.EventObject;

public class FileFoundEvent extends EventObject {
    private static final long serialVersionUID = -1L;
    private Path path;

    public FileFoundEvent(Object source, Path path) {
        super(source);
        this.path = path;
    }

    public Path getPath() {
        return path;
    }

}
