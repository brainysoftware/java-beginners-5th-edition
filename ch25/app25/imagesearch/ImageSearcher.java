/**
 * To run this class (on Windows):
 * 
> cd to the application's bin directory (where all Java classes are compiled to)
> "c:\Program Files\Java\jdk-11\bin\java.exe" --module-path c:\javafx\lib --add-modules javafx.controls app25.imagesearch.ImageSearcher

Note that it is assumed that the JavaFX jar files are located in C:\javafx\lib
 */

package app25.imagesearch;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ImageSearcher extends Application {
    private Executor executor = Executors.newFixedThreadPool(10);
    private AtomicInteger fileNumber = new AtomicInteger();
    private Button searchButton = new Button("Search");
    private ListView<String> imageListView = new ListView<String>();
    private FileFoundListener fileFoundListener = 
            new FileFoundListener() {
        @Override
        public void fileFound(FileFoundEvent event) {
            Platform.runLater(new Runnable() {
                public void run() {
                    String element = fileNumber.incrementAndGet() + ": "
                        + event.getPath().toString();
                    imageListView.getItems().add(0, element);
                }
            });
        }
    };

    @Override
    public void start(Stage stage) {
        VBox root = new VBox(4);
        root.setPadding(new Insets(2, 2, 2, 2));
        ObservableList<Node> children = root.getChildren();
        children.addAll(searchButton, imageListView);
        searchButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Iterable<Path> roots = FileSystems.getDefault()
                        .getRootDirectories();
                searchButton.setDisable(true);
                for (Path root : roots) {
                    ImageSearchTask task = new ImageSearchTask(root, 
                            executor);
                    task.setFileFoundListener(fileFoundListener);
                    executor.execute(task);
                }
            }
        });
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("Image Searcher");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}