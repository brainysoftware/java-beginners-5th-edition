package app25.imagesearch;

import java.util.EventListener;

public interface FileFoundListener extends EventListener {
    void fileFound(FileFoundEvent event);
}
